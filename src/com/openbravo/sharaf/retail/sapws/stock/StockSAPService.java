/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
package com.openbravo.sharaf.retail.sapws.stock;

import java.io.StringWriter;
import java.math.BigDecimal;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.servlet.ServletException;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;

import org.apache.axis.utils.StringUtils;
import org.apache.log4j.Logger;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.utils.FormatUtilities;

import com.openbravo.sharaf.retail.sapws.stock.client.ATPStockCheckOut;
import com.openbravo.sharaf.retail.sapws.stock.client.ATPStockCheckOutService;
import com.openbravo.sharaf.retail.sapws.stock.client.ATPStockCheckRequest;
import com.openbravo.sharaf.retail.sapws.stock.client.ATPStockCheckResponse;

/**
 * Utility class to get available stock of a product in a warehouse from SAP web service.
 * 
 * @author rctoledano
 */

@ApplicationScoped
public class StockSAPService {
  private static Logger log = Logger.getLogger(StockSAPService.class);

  private String user;
  private String passwd;
  private String endpoint;

  ATPStockCheckOutService stockService;
  ATPStockCheckOut stockPort;

  @PostConstruct
  private void initialize() throws ServletException {
    user = null;
    passwd = null;
    endpoint = null;

    OBContext.setAdminMode(false);
    try {
      // Read service configuration
      OBCriteria<SAPSTK_Config> criteria = OBDal.getInstance().createCriteria(SAPSTK_Config.class);
      criteria.addOrderBy(SAPSTK_Config.PROPERTY_LINENO, true);
      criteria.setMaxResults(1);
      SAPSTK_Config config = (SAPSTK_Config) criteria.uniqueResult();
      if (config != null) {
        user = config.getUsername();
        endpoint = config.getEndpoint();
        String password = config.getPassword();
        if (!StringUtils.isEmpty(password)) {
          passwd = FormatUtilities.encryptDecrypt(password, false);
        }
      }

      stockService = new ATPStockCheckOutService();
      stockPort = stockService.getHTTPPort();
      BindingProvider bp = (BindingProvider) stockPort;

      // Set new endpoint
      if (!StringUtils.isEmpty(endpoint)) {
        bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endpoint);
      }

      // Set Basic HTTP Authentication
      if (!StringUtils.isEmpty(user) && !StringUtils.isEmpty(passwd)) {
        bp.getRequestContext().put(BindingProvider.USERNAME_PROPERTY, user);
        bp.getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, passwd);
      }
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  public BigDecimal getStock(final String materialCode, final String siteCode,
      final String storageLocation) {

    // Create the request
    ATPStockCheckRequest stockRequest = new ATPStockCheckRequest();
    ATPStockCheckRequest.ATPRequest stockParam = new ATPStockCheckRequest.ATPRequest();
    stockParam.setMaterialNo(materialCode);
    if (!StringUtils.isEmpty(siteCode)) {
      stockParam.setSiteCode(siteCode);
    }
    if (!StringUtils.isEmpty(storageLocation)) {
      stockParam.setStorageLocation(storageLocation);
    }

    stockRequest.getATPRequest().add(stockParam);
    // Consume service method
    ATPStockCheckResponse stockResponse = stockPort.atpStockCheckOut(stockRequest);
    // log.info("stock response" + stockResponse.getATPResponse().get(0).toString());
    BigDecimal stock = BigDecimal.ZERO;
    if (stockResponse != null) {
      List<ATPStockCheckResponse.ATPResponse> stockList = stockResponse.getATPResponse();
      if (!stockList.isEmpty() && stockList.get(0).getStatus().compareTo("Success") == 0) {
        stock = new BigDecimal(stockList.get(0).getATPStock());
        String[] result = toXML(stockRequest, stockResponse);
        log.info("in getstock method" + result.toString());
      }
      log.info("in getstock method stock value" + stock.toString());

    }
    return stock;
  }

  public static String[] toXML(ATPStockCheckRequest stockRequest,
      ATPStockCheckResponse stockResponse) {
    System.setProperty("com.sun.xml.bind.v2.bytecode.ClassTailor.noOptimize", "true");
    String xml = "";
    String res = "";
    String[] strAr1 = null;
    try {
      JAXBContext jaxbContext = JAXBContext.newInstance(ATPStockCheckRequest.class,
          ATPStockCheckRequest.ATPRequest.class);
      Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
      jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

      JAXBContext Context = JAXBContext.newInstance(ATPStockCheckResponse.class,
          ATPStockCheckResponse.ATPResponse.class);
      Marshaller marshaller = Context.createMarshaller();
      marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

      StringWriter sw = new StringWriter();
      StringWriter sr = new StringWriter();
      QName qName = new QName("http://sharafdg.com/OB/ATPStockCheck", "ATPStockCheck_OutService");
      JAXBElement<ATPStockCheckRequest> root = new JAXBElement<ATPStockCheckRequest>(qName,
          ATPStockCheckRequest.class, stockRequest);
      jaxbMarshaller.marshal(root, sw);
      QName qName2 = new QName("http://sharafdg.com/OB/ATPStockCheck", "ATPStockCheck_OutService");
      JAXBElement<ATPStockCheckResponse> root2 = new JAXBElement<ATPStockCheckResponse>(qName2,
          ATPStockCheckResponse.class, stockResponse);
      marshaller.marshal(root2, sr);

      xml = sw.toString();
      res = sr.toString();
      strAr1 = new String[] { xml, res };
      log.info("stock request" + xml);
      log.info("stock response" + res);
      System.getProperties().remove("com.sun.xml.bind.v2.bytecode.ClassTailor.noOptimize");

    } catch (JAXBException e) {
      log.error("exception", e);
      System.getProperties().remove("com.sun.xml.bind.v2.bytecode.ClassTailor.noOptimize");
    }
    return strAr1;

  }

}
