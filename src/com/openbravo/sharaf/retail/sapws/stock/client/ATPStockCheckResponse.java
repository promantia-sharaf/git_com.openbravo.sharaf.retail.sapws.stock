
package com.openbravo.sharaf.retail.sapws.stock.client;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ATPStockCheckResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ATPStockCheckResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ATPResponse" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="MaterialNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="SiteCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="Stock" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ATPStock" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="UOM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ATPStockCheckResponse", propOrder = {
    "atpResponse"
})
public class ATPStockCheckResponse {

    @XmlElement(name = "ATPResponse")
    protected List<ATPStockCheckResponse.ATPResponse> atpResponse;

    /**
     * Gets the value of the atpResponse property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the atpResponse property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getATPResponse().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ATPStockCheckResponse.ATPResponse }
     * 
     * 
     */
    public List<ATPStockCheckResponse.ATPResponse> getATPResponse() {
        if (atpResponse == null) {
            atpResponse = new ArrayList<ATPStockCheckResponse.ATPResponse>();
        }
        return this.atpResponse;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="MaterialNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="SiteCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="Stock" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATPStock" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="UOM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "materialNo",
        "siteCode",
        "stock",
        "atpStock",
        "uom",
        "status"
    })
    public static class ATPResponse {

        @XmlElement(name = "MaterialNo")
        protected String materialNo;
        @XmlElement(name = "SiteCode")
        protected String siteCode;
        @XmlElement(name = "Stock")
        protected String stock;
        @XmlElement(name = "ATPStock")
        protected String atpStock;
        @XmlElement(name = "UOM")
        protected String uom;
        @XmlElement(name = "Status")
        protected String status;

        /**
         * Gets the value of the materialNo property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMaterialNo() {
            return materialNo;
        }

        /**
         * Sets the value of the materialNo property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMaterialNo(String value) {
            this.materialNo = value;
        }

        /**
         * Gets the value of the siteCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSiteCode() {
            return siteCode;
        }

        /**
         * Sets the value of the siteCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSiteCode(String value) {
            this.siteCode = value;
        }

        /**
         * Gets the value of the stock property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStock() {
            return stock;
        }

        /**
         * Sets the value of the stock property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStock(String value) {
            this.stock = value;
        }

        /**
         * Gets the value of the atpStock property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATPStock() {
            return atpStock;
        }

        /**
         * Sets the value of the atpStock property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATPStock(String value) {
            this.atpStock = value;
        }

        /**
         * Gets the value of the uom property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUOM() {
            return uom;
        }

        /**
         * Sets the value of the uom property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUOM(String value) {
            this.uom = value;
        }

        /**
         * Gets the value of the status property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStatus() {
            return status;
        }

        /**
         * Sets the value of the status property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStatus(String value) {
            this.status = value;
        }

    }

}
