
package com.openbravo.sharaf.retail.sapws.stock.client;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.openbravo.sharaf.retail.sapws.stock.client package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ATPStockCheckResponse_QNAME = new QName("http://sharafdg.com/OB/ATPStockCheck", "ATPStockCheckResponse");
    private final static QName _ATPStockCheckRequest_QNAME = new QName("http://sharafdg.com/OB/ATPStockCheck", "ATPStockCheckRequest");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.openbravo.sharaf.retail.sapws.stock.client
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ATPStockCheckRequest }
     * 
     */
    public ATPStockCheckRequest createATPStockCheckRequest() {
        return new ATPStockCheckRequest();
    }

    /**
     * Create an instance of {@link ATPStockCheckResponse }
     * 
     */
    public ATPStockCheckResponse createATPStockCheckResponse() {
        return new ATPStockCheckResponse();
    }

    /**
     * Create an instance of {@link ATPStockCheckRequest.ATPRequest }
     * 
     */
    public ATPStockCheckRequest.ATPRequest createATPStockCheckRequestATPRequest() {
        return new ATPStockCheckRequest.ATPRequest();
    }

    /**
     * Create an instance of {@link ATPStockCheckResponse.ATPResponse }
     * 
     */
    public ATPStockCheckResponse.ATPResponse createATPStockCheckResponseATPResponse() {
        return new ATPStockCheckResponse.ATPResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ATPStockCheckResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sharafdg.com/OB/ATPStockCheck", name = "ATPStockCheckResponse")
    public JAXBElement<ATPStockCheckResponse> createATPStockCheckResponse(ATPStockCheckResponse value) {
        return new JAXBElement<ATPStockCheckResponse>(_ATPStockCheckResponse_QNAME, ATPStockCheckResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ATPStockCheckRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sharafdg.com/OB/ATPStockCheck", name = "ATPStockCheckRequest")
    public JAXBElement<ATPStockCheckRequest> createATPStockCheckRequest(ATPStockCheckRequest value) {
        return new JAXBElement<ATPStockCheckRequest>(_ATPStockCheckRequest_QNAME, ATPStockCheckRequest.class, null, value);
    }

}
