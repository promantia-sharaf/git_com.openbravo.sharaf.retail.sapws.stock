package com.openbravo.sharaf.retail.sapws.stock.client;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for ATPStockCheckRequest complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ATPStockCheckRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ATPRequest" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="MaterialNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="SiteCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="StorageLocation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ATPStockCheckRequest", propOrder = { "atpRequest" })
public class ATPStockCheckRequest {

  @XmlElement(name = "ATPRequest")
  protected List<ATPStockCheckRequest.ATPRequest> atpRequest;

  /**
   * Gets the value of the atpRequest property.
   * 
   * <p>
   * This accessor method returns a reference to the live list, not a snapshot. Therefore any
   * modification you make to the returned list will be present inside the JAXB object. This is why
   * there is not a <CODE>set</CODE> method for the atpRequest property.
   * 
   * <p>
   * For example, to add a new item, do as follows:
   * 
   * <pre>
   * getATPRequest().add(newItem);
   * </pre>
   * 
   * 
   * <p>
   * Objects of the following type(s) are allowed in the list
   * {@link ATPStockCheckRequest.ATPRequest }
   * 
   * 
   */
  public List<ATPStockCheckRequest.ATPRequest> getATPRequest() {
    if (atpRequest == null) {
      atpRequest = new ArrayList<ATPStockCheckRequest.ATPRequest>();
    }
    return this.atpRequest;
  }

  /**
   * <p>
   * Java class for anonymous complex type.
   * 
   * <p>
   * The following schema fragment specifies the expected content contained within this class.
   * 
   * <pre>
   * &lt;complexType>
   *   &lt;complexContent>
   *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
   *       &lt;sequence>
   *         &lt;element name="MaterialNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
   *         &lt;element name="SiteCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
   *         &lt;element name="StorageLocation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
   *       &lt;/sequence>
   *     &lt;/restriction>
   *   &lt;/complexContent>
   * &lt;/complexType>
   * </pre>
   * 
   * 
   */
  @XmlAccessorType(XmlAccessType.FIELD)
  @XmlType(name = "", propOrder = { "materialNo", "siteCode", "storageLocation" })
  public static class ATPRequest {

    @XmlElement(name = "MaterialNo")
    protected String materialNo;
    @XmlElement(name = "SiteCode")
    protected String siteCode;
    @XmlElement(name = "StorageLocation")
    protected String storageLocation;

    /**
     * Gets the value of the materialNo property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getMaterialNo() {
      return materialNo;
    }

    /**
     * Sets the value of the materialNo property.
     * 
     * @param value
     *          allowed object is {@link String }
     * 
     */
    public void setMaterialNo(String value) {
      this.materialNo = value;
    }

    /**
     * Gets the value of the siteCode property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getSiteCode() {
      return siteCode;
    }

    /**
     * Sets the value of the siteCode property.
     * 
     * @param value
     *          allowed object is {@link String }
     * 
     */
    public void setSiteCode(String value) {
      this.siteCode = value;
    }

    /**
     * Gets the value of the storageLocation property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getStorageLocation() {
      return storageLocation;
    }

    /**
     * Sets the value of the storageLocation property.
     * 
     * @param value
     *          allowed object is {@link String }
     * 
     */
    public void setStorageLocation(String value) {
      this.storageLocation = value;
    }

  }

}
